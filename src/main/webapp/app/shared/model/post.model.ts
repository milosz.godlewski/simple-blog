export interface IPost {
    id?: string;
    titile?: string;
    content?: string;
}

export class Post implements IPost {
    constructor(public id?: string, public titile?: string, public content?: string) {}
}
