/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mgodlewski.web.rest.vm;
